
// Creates data attributes from the titles of the custom tag buttons for CSS tooltips
var updateTooltips = function() {
    jQuery('.ae-button.ez-btn-ae').each(function() {
        var iconTitle = jQuery(this).attr('title');
        jQuery(this).attr('data-tooltip', iconTitle);
    });
}
jQuery('body').on('DOMSubtreeModified', '.ae-ui', function() {
    updateTooltips();
});
